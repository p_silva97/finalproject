package com.example.android.myrates.core.network;

import android.os.AsyncTask;

public class FetchDataTask extends AsyncTask<Integer, Void, String> {

    private SimpleTask simpleTask;

    public FetchDataTask(SimpleTask simpleTask) {
        this.simpleTask = simpleTask;
    }

    @Override
    protected String doInBackground(Integer... integers) {
        return simpleTask.run();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        simpleTask.workDone(s);
    }
}
