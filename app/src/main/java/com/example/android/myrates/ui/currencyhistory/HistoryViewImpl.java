package com.example.android.myrates.ui.currencyhistory;

import com.example.android.myrates.ui.model.CurrencyRateAndDateRetrieved;
import com.example.android.myrates.ui.model.CurrencyRateItem;

import java.util.ArrayList;

public interface HistoryViewImpl {

    void setCurrencyRateItem(CurrencyRateItem currencyRateItem);

    void onHistoryRetrieved(ArrayList<CurrencyRateAndDateRetrieved> currencyRateItems);

}
