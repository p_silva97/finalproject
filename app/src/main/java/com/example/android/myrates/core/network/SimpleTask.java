package com.example.android.myrates.core.network;

public interface SimpleTask {

    String run();

    void workDone(String s);

}
