package com.example.android.myrates.utils;

import android.net.Uri;

import com.example.android.myrates.core.ApiEndpoints;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public final class BuildUrlFromCurrency {

    private BuildUrlFromCurrency() {
        throw new UnsupportedOperationException();
    }

    public static URL buildLatestRatesUrl(String currencyName) {

        URL url;

        Uri uri = Uri.parse(ApiEndpoints.BASE_URL).buildUpon()
                .appendPath(ApiEndpoints.LATEST_RATES)
                .appendQueryParameter(ApiEndpoints.QUERY_PARAM_BASE_CURRENCY, currencyName)
                .build();

        try {
            url = new URL(uri.toString());
            return url;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static URL buildHistoricalRatesUrl(String currencyName) {

        URL url;
        String firstDayOfTheYear = Calendar.getInstance().get(Calendar.YEAR) + "-01-01";
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

        Uri uri = Uri.parse(ApiEndpoints.BASE_URL).buildUpon()
                .appendPath(ApiEndpoints.HISTORICAL_RATES)
                .appendQueryParameter(ApiEndpoints.QUERY_PARAM_START_DATE, firstDayOfTheYear)
                .appendQueryParameter(ApiEndpoints.QUERY_PARAM_END_DATE, currentDate)
                .appendQueryParameter(ApiEndpoints.QUERY_PARAM_COMPARE_CURRENCY, currencyName)
                .build();

        try {
            url = new URL(uri.toString());
            return url;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return null;

    }

    public static URL historicalRatesFromGivenDates(String currencyName, String initialDate, String finalDate) {
        URL url;

        Uri uri = Uri.parse(ApiEndpoints.BASE_URL).buildUpon()
                .appendPath(ApiEndpoints.HISTORICAL_RATES)
                .appendQueryParameter(ApiEndpoints.QUERY_PARAM_START_DATE, initialDate)
                .appendQueryParameter(ApiEndpoints.QUERY_PARAM_END_DATE, finalDate)
                .appendQueryParameter(ApiEndpoints.QUERY_PARAM_COMPARE_CURRENCY, currencyName)
                .build();

        try {
            url = new URL(uri.toString());
            return url;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return null;
    }

}
