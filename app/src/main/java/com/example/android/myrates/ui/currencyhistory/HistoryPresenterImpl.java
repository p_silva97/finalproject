package com.example.android.myrates.ui.currencyhistory;

import com.example.android.myrates.ui.model.CurrencyRateAndDateRetrieved;
import com.example.android.myrates.ui.model.CurrencyRateItem;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;

public interface HistoryPresenterImpl {

    void fetchRatesHistoryForSelectedCurrency();

    void fetchRatesHistoryFromGivenDates(String initialDate, String finalDate);

    String getInformationFromPointOnGraph(float dayOfRate, float rateValue);

    long showSelectedRateOnGraph(CurrencyRateAndDateRetrieved rateAndDateRetrieved);

    void sortHistoryRatesByDate(ArrayList<CurrencyRateAndDateRetrieved> arrayRates);

    void addStatusAccordingToRate(ArrayList<CurrencyRateAndDateRetrieved> arrayRates);

    ArrayList<Entry> createDataSetFromRates(ArrayList<CurrencyRateAndDateRetrieved> arrayRates);

}