package com.example.android.myrates.core.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.Map;

public class JsonHistoryStructure {

    @SerializedName("rates")
    @Expose
    private Map<String, Map<String, Double>> rates;
    @SerializedName("start_at")
    @Expose
    private String startAt;
    @SerializedName("base")
    @Expose
    private String base;
    @SerializedName("end_at")
    @Expose
    private String endAt;

    public Map<String, Map<String, Double>> getRates() {
        return rates;
    }

    public void setRates(Map<String, Map<String, Double>> rates) {
        this.rates = rates;
    }

    public String getStartAt() {
        return startAt;
    }

    public void setStartAt(String startAt) {
        this.startAt = startAt;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getEndAt() {
        return endAt;
    }

    public void setEndAt(String endAt) {
        this.endAt = endAt;
    }

}
