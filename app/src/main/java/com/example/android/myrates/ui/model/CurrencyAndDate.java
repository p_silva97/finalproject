package com.example.android.myrates.ui.model;

public class CurrencyAndDate {

    private String defaultCurrency, dateInformationRetrieved;

    public CurrencyAndDate() {
        this("", "");
    }

    private CurrencyAndDate(String defaultCurrency, String dateInformationRetrieved) {
        this.defaultCurrency = defaultCurrency;
        this.dateInformationRetrieved = dateInformationRetrieved;
    }

    public String getDefaultCurrency() {
        return defaultCurrency;
    }

    public void setDefaultCurrency(String defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

    public String getDateInformationRetrieved() {
        return dateInformationRetrieved;
    }

    public void setDateInformationRetrieved(String dateInformationRetrieved) {
        this.dateInformationRetrieved = dateInformationRetrieved;
    }

}
