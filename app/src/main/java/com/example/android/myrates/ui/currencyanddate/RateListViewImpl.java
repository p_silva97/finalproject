package com.example.android.myrates.ui.currencyanddate;

import com.example.android.myrates.ui.model.CurrencyAndDate;
import com.example.android.myrates.ui.model.CurrencyRateItem;

import java.util.ArrayList;

public interface RateListViewImpl {

    void onRatesListSuccess(CurrencyAndDate currencyAndDate, ArrayList<CurrencyRateItem> currencyRateItems);

    void onRatesListFail(String errorMessage);

}
