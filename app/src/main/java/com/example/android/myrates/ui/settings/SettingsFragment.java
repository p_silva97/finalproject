package com.example.android.myrates.ui.settings;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.example.android.myrates.R;
import com.example.android.myrates.ui.currencyanddate.RateListPresenter;
import com.example.android.myrates.ui.model.CurrencyAndDate;

public class SettingsFragment extends PreferenceFragmentCompat implements SettingsViewImpl {

    private ListPreference listPreference;
    private CurrencyAndDate currencyAndDate;

    private SettingsPresenterImpl settingsPresenter;

    public void setCurrencyAndDate(CurrencyAndDate currencyAndDate) {
        this.currencyAndDate = currencyAndDate;
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.settings, rootKey);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        settingsPresenter = new SettingsPresenter(this);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {

        listPreference = findPreference("currency");
        listPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                settingsPresenter.setSelectedCurrencyAsDefault(currencyAndDate, newValue);
                return true;
            }
        });

    }

}
