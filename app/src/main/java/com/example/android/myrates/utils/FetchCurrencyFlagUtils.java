package com.example.android.myrates.utils;

import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;

import com.example.android.myrates.R;

public final class FetchCurrencyFlagUtils {

    private FetchCurrencyFlagUtils() {
        throw new UnsupportedOperationException();
    }

    public static void loadImageCurrency(ImageView imageView, String currency) {

        if (imageView == null || TextUtils.isEmpty(currency)) {
            return;
        }

        String uri = "@drawable/ic_" + currency.toLowerCase();
        int imageResource = imageView.getResources().getIdentifier(uri, null, imageView.getContext().getPackageName());

        if (imageResource == 0) {
            Drawable res = imageView.getResources().getDrawable(R.drawable.ic_euro_symbol);
            imageView.setImageDrawable(res);
        } else {
            Drawable res = imageView.getResources().getDrawable(imageResource);
            imageView.setImageDrawable(res);
        }

    }

}
