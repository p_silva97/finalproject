package com.example.android.myrates.ui.settings;

import com.example.android.myrates.ui.model.CurrencyAndDate;

public interface SettingsPresenterImpl {

    void setSelectedCurrencyAsDefault(CurrencyAndDate currencyAndDate, Object newCurrency);

}
