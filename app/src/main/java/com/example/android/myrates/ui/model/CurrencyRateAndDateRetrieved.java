package com.example.android.myrates.ui.model;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CurrencyRateAndDateRetrieved {

    private String currency, dateRetrieved;
    private double currencyValue;
    private int rateStatus;

    public CurrencyRateAndDateRetrieved(String currency, String dateRetrieved, double currencyValue, int rateStatus) {
        this.currency = currency;
        this.dateRetrieved = dateRetrieved;
        this.currencyValue = currencyValue;
        this.rateStatus = rateStatus;
    }

    public String getCurrency() {
        return currency;
    }

    public String getDateRetrieved() {
        return dateRetrieved;
    }

    public double getCurrencyValue() {
        return currencyValue;
    }

    public int getRateStatus() {
        return rateStatus;
    }

    public void setRateStatus(int rateStatus) {
        this.rateStatus = rateStatus;
    }

    @SuppressLint("SimpleDateFormat")
    public Date getDateRetrievedAsDate(String dateFormat) throws ParseException {
        return new SimpleDateFormat(dateFormat).parse(getDateRetrieved());
    }

}
