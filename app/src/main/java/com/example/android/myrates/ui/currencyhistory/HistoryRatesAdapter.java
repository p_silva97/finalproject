package com.example.android.myrates.ui.currencyhistory;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.myrates.R;
import com.example.android.myrates.ui.model.CurrencyRateAndDateRetrieved;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class HistoryRatesAdapter extends RecyclerView.Adapter<HistoryRatesAdapter.HistoryListViewHolder> {

    private int selectedPos = RecyclerView.NO_POSITION, layoutId;

    private List<CurrencyRateAndDateRetrieved> historyRatesList;

    private RecyclerView recyclerView;

    private RateClickListener rateClickListener;

    HistoryRatesAdapter(List<CurrencyRateAndDateRetrieved> historyRatesList, int layoutId) {
        this.historyRatesList = historyRatesList;
        this.layoutId = layoutId;
    }

    @NonNull
    @Override
    public HistoryListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new HistoryRatesAdapter.HistoryListViewHolder(view, rateClickListener);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryListViewHolder holder, int position) {
        CurrencyRateAndDateRetrieved currentItem = historyRatesList.get(position);

        DecimalFormat decimalFormat = new DecimalFormat("#.####");

        String currencyDate = currentItem.getDateRetrieved();
        double currencyVal = currentItem.getCurrencyValue();

        holder.dateTextView.setText(currencyDate);
        holder.currencyValueTextView.setText(decimalFormat.format(currencyVal));

        //aumentou a rate
        if (currentItem.getRateStatus() == 1) {
            holder.rateIndicatorImageView.setImageResource(R.drawable.ic_trending_up);
            holder.rateIndicatorImageView.setColorFilter(Color.GREEN);
            holder.rateIndicatorImageView.setVisibility(View.VISIBLE);
        }

        //mateve a rate
        else if (currentItem.getRateStatus() == 2) {
            holder.rateIndicatorImageView.setImageResource(R.drawable.ic_trending_flat);
            holder.rateIndicatorImageView.setColorFilter(Color.YELLOW);
            holder.rateIndicatorImageView.setVisibility(View.VISIBLE);
        }

        //diminui a rate
        else if (currentItem.getRateStatus() == 3) {
            holder.rateIndicatorImageView.setImageResource(R.drawable.ic_trending_down);
            holder.rateIndicatorImageView.setColorFilter(Color.RED);
            holder.rateIndicatorImageView.setVisibility(View.VISIBLE);
        } else {
            holder.rateIndicatorImageView.setVisibility(View.INVISIBLE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            holder.itemView.setForeground(selectedPos == position ? holder.itemView.getContext().getDrawable(R.drawable.currency_selected) : null);
        }

    }

    @Override
    public int getItemCount() {
        return historyRatesList.size();
    }

    class HistoryListViewHolder extends RecyclerView.ViewHolder {

        TextView dateTextView, currencyValueTextView;
        ImageView rateIndicatorImageView;

        HistoryListViewHolder(@NonNull View itemView, final RateClickListener clickListener) {
            super(itemView);

            dateTextView = itemView.findViewById(R.id.currency_date);
            currencyValueTextView = itemView.findViewById(R.id.history_currency_value);
            rateIndicatorImageView = itemView.findViewById(R.id.rate_indicator);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (selectedPos == getAdapterPosition()) {
                        selectedPos = RecyclerView.NO_POSITION;
                        notifyItemChanged(getAdapterPosition());
                    } else {
                        int oldPosition = selectedPos;
                        selectedPos = getLayoutPosition();
                        notifyItemChanged(selectedPos);
                        notifyItemChanged(oldPosition);
                    }

                    if (clickListener != null) {
                        CurrencyRateAndDateRetrieved currency = historyRatesList.get(getAdapterPosition());

                        if (getAdapterPosition() != RecyclerView.NO_POSITION)
                            clickListener.onRateClicked(currency);

                    }

                }
            });

        }

    }

    public void setSelectedItemByDate(float floatDate) {

        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = dateFormat.format(new Date((long) floatDate));
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date((long) floatDate));

        if (calendar.get(Calendar.HOUR) == 11) {
            calendar.add(Calendar.HOUR, 1);
            formattedDate = dateFormat.format(new Date(calendar.getTimeInMillis()));
        }
        int position = 0;

        for (int i = 0; i < historyRatesList.size(); i++) {

            if (historyRatesList.get(i).getDateRetrieved().equals(formattedDate)) {
                position = i;
                break;
            }

        }

        int oldPosition = selectedPos;
        selectedPos = position;
        notifyItemChanged(selectedPos);
        notifyItemChanged(oldPosition);

        recyclerView.scrollToPosition(position);

    }

    public interface RateClickListener {
        void onRateClicked(CurrencyRateAndDateRetrieved currency);
    }

    void setRateClickListener(HistoryRatesAdapter.RateClickListener listener) {
        rateClickListener = listener;
    }

}
