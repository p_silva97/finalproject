package com.example.android.myrates.ui.currencycalculator;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.myrates.R;
import com.example.android.myrates.ui.model.CurrencyRateItem;
import com.example.android.myrates.utils.FetchCurrencyFlagUtils;

import java.text.DecimalFormat;
import java.util.List;

public class CalculatedRatesAdapter extends RecyclerView.Adapter<CalculatedRatesAdapter.CalculatorListViewHolder> {

    private List<CurrencyRateItem> calculatedRatesList;

    private int layoutId;

    CalculatedRatesAdapter(List<CurrencyRateItem> calculatedList, int layoutId) {
        calculatedRatesList = calculatedList;
        this.layoutId = layoutId;
    }

    @NonNull
    @Override
    public CalculatorListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new CalculatedRatesAdapter.CalculatorListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CalculatorListViewHolder holder, int position) {
        CurrencyRateItem currentItem = calculatedRatesList.get(position);

        DecimalFormat decimalFormat = new DecimalFormat("#.####");

        String currencyName = currentItem.getCurrency();
        double currencyVal = currentItem.getCurrencyValue();

        FetchCurrencyFlagUtils.loadImageCurrency(holder.currencyFlagImageView, currencyName);
        holder.currencyTextView.setText(currencyName);
        holder.currencyValueTextView.setText(decimalFormat.format(currencyVal));
    }

    @Override
    public int getItemCount() {
        return calculatedRatesList.size();
    }

    static class CalculatorListViewHolder extends RecyclerView.ViewHolder {

        TextView currencyTextView, currencyValueTextView;
        ImageView currencyFlagImageView;

        CalculatorListViewHolder(@NonNull View itemView) {
            super(itemView);

            currencyTextView = itemView.findViewById(R.id.currency_name);
            currencyValueTextView = itemView.findViewById(R.id.currency_value);
            currencyFlagImageView = itemView.findViewById(R.id.currency_image);

        }
    }
}
