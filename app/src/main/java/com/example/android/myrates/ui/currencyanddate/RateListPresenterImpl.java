package com.example.android.myrates.ui.currencyanddate;

import com.example.android.myrates.ui.model.CurrencyRateItem;

import java.util.ArrayList;

public interface RateListPresenterImpl {

    CurrencyRateItem getCurrentlySelectedItem();

    void setCurrentlySelectedItem(CurrencyRateItem currentlySelectedItem);

    String getBaseCurrency();

    String getInformationDate();

    void getLatestRateList();

    ArrayList<CurrencyRateItem> loadFavoriteList();

    void removeOrAddToFavorites(CurrencyRateItem currencyRateItem);

    void serializeArrayList();

}
