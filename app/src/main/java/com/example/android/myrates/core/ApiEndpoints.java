package com.example.android.myrates.core;

public class ApiEndpoints {

    public static final String BASE_URL = "https://api.exchangeratesapi.io/";

    public static final String LATEST_RATES = "latest";
    public static final String HISTORICAL_RATES = "history";

    public static final String QUERY_PARAM_BASE_CURRENCY = "base";
    public static final String QUERY_PARAM_COMPARE_CURRENCY = "symbols";
    public static final String QUERY_PARAM_START_DATE = "start_at";
    public static final String QUERY_PARAM_END_DATE = "end_at";

}
