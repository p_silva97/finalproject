package com.example.android.myrates.ui.model;

public class CurrencyRateItem {

    private String currency;
    private double currencyValue;
    private boolean currencyFavorite;

    public CurrencyRateItem(String currency, double currencyValue, boolean currencyFavorite) {
        this.currency = currency;
        this.currencyValue = currencyValue;
        this.currencyFavorite = currencyFavorite;
    }

    public String getCurrency() {
        return currency;
    }

    public double getCurrencyValue() {
        return currencyValue;
    }

    public boolean isCurrencyFavorite() {
        return currencyFavorite;
    }

    public void setCurrencyFavorite(boolean currencyFavorite) {
        this.currencyFavorite = currencyFavorite;
    }
}
