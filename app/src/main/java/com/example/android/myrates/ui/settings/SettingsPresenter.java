package com.example.android.myrates.ui.settings;

import com.example.android.myrates.ui.model.CurrencyAndDate;

public class SettingsPresenter implements SettingsPresenterImpl {

    private SettingsViewImpl settingsView;

    public SettingsPresenter(SettingsViewImpl settingsView) {
        this.settingsView = settingsView;
    }

    @Override
    public void setSelectedCurrencyAsDefault(CurrencyAndDate currencyAndDate, Object newCurrency) {
        currencyAndDate.setDefaultCurrency(newCurrency.toString());
    }

}
