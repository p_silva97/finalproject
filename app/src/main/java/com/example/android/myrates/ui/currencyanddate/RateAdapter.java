package com.example.android.myrates.ui.currencyanddate;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.myrates.R;
import com.example.android.myrates.ui.model.CurrencyRateItem;
import com.example.android.myrates.utils.FetchCurrencyFlagUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class RateAdapter extends RecyclerView.Adapter<RateAdapter.RateListViewHolder> implements Filterable{

    private List<CurrencyRateItem> currencyRateItems, currencyRateItemsFull;
    private int selectedPos = RecyclerView.NO_POSITION, layoutId;

    private RateClickListener rateClickListener;

    RateAdapter(List<CurrencyRateItem> rateList, int layoutId) {
        currencyRateItems = rateList;
        currencyRateItemsFull = new ArrayList<>(currencyRateItems);
        this.layoutId = layoutId;
    }

    @NonNull
    @Override
    public RateListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new RateListViewHolder(view, rateClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RateListViewHolder holder, int position) {
        CurrencyRateItem currentItem = currencyRateItems.get(position);

        DecimalFormat decimalFormat = new DecimalFormat("#.####");

        String currencyName = currentItem.getCurrency();
        double currencyVal = currentItem.getCurrencyValue();

        FetchCurrencyFlagUtils.loadImageCurrency(holder.currencyFlagImageView, currencyName);
        holder.currencyTextView.setText(currencyName);
        holder.currencyValueTextView.setText(decimalFormat.format(currencyVal));

        if (currentItem.isCurrencyFavorite())
            holder.favoriteItemImageView.setVisibility(View.VISIBLE);
        else
            holder.favoriteItemImageView.setVisibility(View.INVISIBLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            holder.itemView.setForeground(selectedPos == position ? holder.itemView.getContext().getDrawable(R.drawable.currency_selected) : null);
        }
    }

    @Override
    public int getItemCount() {
        return currencyRateItems.size();
    }

    @Override
    public Filter getFilter() {
        return currencyFilter;
    }

    private Filter currencyFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<CurrencyRateItem> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(currencyRateItemsFull);
            }

            //adicionar aqui a condição quando o layout é alterado

            else {
                String searchEntry = constraint.toString().toLowerCase().trim();

                for (CurrencyRateItem item : currencyRateItemsFull) {

                    if (item.getCurrency().toLowerCase().contains(searchEntry)) {
                        filteredList.add(item);
                    }

                }

            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            currencyRateItems.clear();
            currencyRateItems.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    class RateListViewHolder extends RecyclerView.ViewHolder {

        TextView currencyTextView, currencyValueTextView;
        ImageView currencyFlagImageView, favoriteItemImageView;
        CardView currencyCardView;

        RateListViewHolder(@NonNull final View itemView, final RateClickListener clickListener) {
            super(itemView);

            currencyTextView = itemView.findViewById(R.id.currency_name);
            currencyValueTextView = itemView.findViewById(R.id.currency_value);
            currencyFlagImageView = itemView.findViewById(R.id.currency_image);
            favoriteItemImageView = itemView.findViewById(R.id.favorite_indicator);
            currencyCardView = itemView.findViewById(R.id.currency_card);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    boolean navigationBarActive;

                    if (selectedPos == getAdapterPosition()) {
                        selectedPos = RecyclerView.NO_POSITION;
                        notifyItemChanged(getAdapterPosition());
                        navigationBarActive = false;
                    } else {
                        int oldPosition = selectedPos;
                        selectedPos = getLayoutPosition();
                        notifyItemChanged(selectedPos);
                        notifyItemChanged(oldPosition);
                        navigationBarActive = true;
                    }

                    if (clickListener != null) {
                        CurrencyRateItem currency = currencyRateItems.get(getAdapterPosition());

                        if (getAdapterPosition() != RecyclerView.NO_POSITION)
                            clickListener.onRateClicked(currency, navigationBarActive);

                    }

                }

            });

        }

    }

    public interface RateClickListener {
        void onRateClicked(CurrencyRateItem currency, boolean showNavigationBar);
    }

    void setRateClickListener(RateClickListener listener) {
        rateClickListener = listener;
    }

}
