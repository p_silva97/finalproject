package com.example.android.myrates.core;

import android.app.Application;
import android.content.Context;

public class SingletonContext extends Application {

    private static SingletonContext instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    private Context getContext() {
        return this;
    }

    public static Context get() {
        return getInstance().getContext();
    }

    public static SingletonContext getInstance() {
        return instance;
    }

}
