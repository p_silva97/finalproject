package com.example.android.myrates.ui.currencyhistory;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.TransitionManager;

import com.example.android.myrates.R;
import com.example.android.myrates.ui.model.CurrencyRateAndDateRetrieved;
import com.example.android.myrates.ui.model.CurrencyRateItem;
import com.example.android.myrates.utils.FetchCurrencyFlagUtils;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.StringTokenizer;

public class HistoryFragment extends Fragment implements HistoryViewImpl {

    private CurrencyRateItem currencyRateItem;

    private LineChart currencyRatesLineChart;
    private TextView selectedGraphRateTextView, initialDateTextView, finalDateTextView;
    private ImageView currencyHistoryImageView, expandImageView;
    private RecyclerView historyRecyclerView;

    private HistoryPresenterImpl historyPresenter;
    private HistoryRatesAdapter historyRatesAdapter;

    private ViewGroup rootView;

    public void setCurrencyRateItem(CurrencyRateItem currencyRateItem) {
        this.currencyRateItem = currencyRateItem;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_history, container, false);
        historyPresenter = new HistoryPresenter(this, currencyRateItem);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        historyPresenter.fetchRatesHistoryForSelectedCurrency();
    }

    @SuppressLint("SimpleDateFormat")
    private void initViews() {

        selectedGraphRateTextView = Objects.requireNonNull(getView()).findViewById(R.id.selected_chart_rate);

        expandImageView = Objects.requireNonNull(getView()).findViewById(R.id.expand_icon);
        expandImageView.setSelected(false);
        expandImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int layoutId;

                if (!expandImageView.isSelected()) {
                    layoutId = R.layout.fragment_history_expand_open;
                    expandImageView.setSelected(true);
                } else {
                    layoutId = R.layout.fragment_history_expand_closed;
                    expandImageView.setSelected(false);
                }

                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(getContext(), layoutId);
                TransitionManager.beginDelayedTransition(rootView);
                constraintSet.applyTo((ConstraintLayout) rootView);
            }
        });

        currencyRatesLineChart = Objects.requireNonNull(getView()).findViewById(R.id.line_chart);
        currencyRatesLineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                float firstVal = e.getX();
                float secondVal = e.getY();
                selectedGraphRateTextView.setVisibility(View.VISIBLE);
                selectedGraphRateTextView.setText(historyPresenter.getInformationFromPointOnGraph(firstVal, secondVal));
                historyRatesAdapter.setSelectedItemByDate(firstVal);
            }

            @Override
            public void onNothingSelected() {
                selectedGraphRateTextView.setVisibility(View.INVISIBLE);
            }
        });

        currencyHistoryImageView = Objects.requireNonNull(getView()).findViewById(R.id.history_currency);
        FetchCurrencyFlagUtils.loadImageCurrency(currencyHistoryImageView, currencyRateItem.getCurrency());

        initialDateTextView = Objects.requireNonNull(getView()).findViewById(R.id.initial_date);
        initialDateTextView.setText(new SimpleDateFormat("yyyy-MM-dd").format(fetchDate(true).getTime()));
        initialDateTextView.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SimpleDateFormat")
            @Override
            public void onClick(View v) {
                StringTokenizer tokens = new StringTokenizer(initialDateTextView.getText().toString(), "-");
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        Objects.requireNonNull(getContext()),
                        android.R.style.Theme_Material_Dialog_MinWidth,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                String date = year + "-" + (month + 1) + "-" + dayOfMonth;
                                initialDateTextView.setText(date);
                                historyPresenter.fetchRatesHistoryFromGivenDates(date, finalDateTextView.getText().toString());
                            }
                        },
                        Integer.parseInt(tokens.nextToken()),
                        Integer.parseInt(tokens.nextToken()) - 1,
                        Integer.parseInt(tokens.nextToken()));
                datePickerDialog.getDatePicker().setMinDate(fetchDate(true).getTime().getTime());
                try {
                    datePickerDialog.getDatePicker().setMaxDate(Objects.requireNonNull(new SimpleDateFormat("yyyy-MM-dd").parse(finalDateTextView.getText().toString())).getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                datePickerDialog.show();
            }
        });

        finalDateTextView = Objects.requireNonNull(getView()).findViewById(R.id.final_date);
        finalDateTextView.setText(new SimpleDateFormat("yyyy-MM-dd").format(fetchDate(false).getTime()));
        finalDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringTokenizer tokens = new StringTokenizer(finalDateTextView.getText().toString(), "-");
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        Objects.requireNonNull(getContext()),
                        android.R.style.Theme_Material_Dialog_MinWidth,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                month = month + 1;
                                String date = year + "-" + month + "-" + dayOfMonth;
                                finalDateTextView.setText(date);
                                historyPresenter.fetchRatesHistoryFromGivenDates(initialDateTextView.getText().toString(), date);
                            }
                        },
                        Integer.parseInt(tokens.nextToken()),
                        Integer.parseInt(tokens.nextToken()) - 1,
                        Integer.parseInt(tokens.nextToken()));
                try {
                    datePickerDialog.getDatePicker().setMinDate(Objects.requireNonNull(new SimpleDateFormat("yyyy-MM-dd").parse(initialDateTextView.getText().toString())).getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                datePickerDialog.getDatePicker().setMaxDate(fetchDate(false).getTime().getTime());
                datePickerDialog.show();
            }
        });

        historyRecyclerView = getView().findViewById(R.id.calculator_rates);
        historyRecyclerView.setHasFixedSize(false);
        historyRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    public void onHistoryRetrieved(ArrayList<CurrencyRateAndDateRetrieved> currencyRateItems) {
        createAdapter(currencyRateItems);
        createRatesGraph(currencyRateItems);
    }

    private void createAdapter(ArrayList<CurrencyRateAndDateRetrieved> currencyRateItems) {
        historyRatesAdapter = new HistoryRatesAdapter(currencyRateItems, R.layout.history_list_item);
        historyRecyclerView.setAdapter(historyRatesAdapter);
        historyRatesAdapter.setRateClickListener(new HistoryRatesAdapter.RateClickListener() {
            @Override
            public void onRateClicked(CurrencyRateAndDateRetrieved currency) {
                currencyRatesLineChart.highlightValue(historyPresenter.showSelectedRateOnGraph(currency), 0);
            }
        });
    }

    private void createRatesGraph(ArrayList<CurrencyRateAndDateRetrieved> currencyRateItems) {

        LineDataSet lineDataSet = new LineDataSet(historyPresenter.createDataSetFromRates(currencyRateItems), "Currency Rates");
        ArrayList<ILineDataSet> iLineDataSets = new ArrayList<>();
        iLineDataSets.add(lineDataSet);
        LineData lineData = new LineData(iLineDataSets);

        //look and feel
        lineDataSet.setDrawFilled(true);
        lineDataSet.setDrawValues(false);
        currencyRatesLineChart.getDescription().setEnabled(false);

        currencyRatesLineChart.getXAxis().setDrawLabels(true);
        currencyRatesLineChart.getXAxis().setDrawGridLines(false);
        currencyRatesLineChart.getXAxis().setDrawAxisLine(false);
        ValueFormatter iAxisValueFormatter = new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                Date date = new Date((long)value);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("MMM");
                return dateFormat.format(date).toUpperCase();
            }
        };
        currencyRatesLineChart.getXAxis().setValueFormatter(iAxisValueFormatter);

        currencyRatesLineChart.getAxisLeft().setDrawLabels(false);
        currencyRatesLineChart.getAxisLeft().setDrawGridLines(false);
        currencyRatesLineChart.getAxisLeft().setDrawAxisLine(false);

        currencyRatesLineChart.getAxisRight().setDrawLabels(false);
        currencyRatesLineChart.getAxisRight().setDrawGridLines(false);
        currencyRatesLineChart.getAxisRight().setDrawAxisLine(false);

        //functionality
        currencyRatesLineChart.setPinchZoom(true);

        //data set
        currencyRatesLineChart.setData(lineData);
        currencyRatesLineChart.invalidate();

    }

    private Calendar fetchDate(boolean firstOrLastDay) {

        if (firstOrLastDay) {
            Calendar calendarStart = Calendar.getInstance();
            calendarStart.get(Calendar.YEAR);
            calendarStart.set(Calendar.MONTH, 0);
            calendarStart.set(Calendar.DAY_OF_MONTH, 1);
            return calendarStart;
        } else {
            Calendar calendarEnd = Calendar.getInstance();
            calendarEnd.get(Calendar.YEAR);
            calendarEnd.get(Calendar.MONTH);
            calendarEnd.get(Calendar.DAY_OF_MONTH);
            return calendarEnd;
        }

    }

}
