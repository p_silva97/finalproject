package com.example.android.myrates.ui.settings;

import com.example.android.myrates.ui.model.CurrencyAndDate;

public interface SettingsViewImpl {

    void setCurrencyAndDate(CurrencyAndDate currencyAndDate);

}
