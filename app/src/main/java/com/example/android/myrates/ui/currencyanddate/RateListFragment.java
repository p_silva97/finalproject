package com.example.android.myrates.ui.currencyanddate;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.TransitionManager;

import com.example.android.myrates.R;
import com.example.android.myrates.ui.currencycalculator.CalculatorFragment;
import com.example.android.myrates.ui.currencycalculator.CalculatorViewImpl;
import com.example.android.myrates.ui.currencyhistory.HistoryFragment;
import com.example.android.myrates.ui.currencyhistory.HistoryViewImpl;
import com.example.android.myrates.ui.settings.SettingsFragment;
import com.example.android.myrates.ui.settings.SettingsViewImpl;
import com.example.android.myrates.utils.FetchCurrencyFlagUtils;
import com.example.android.myrates.ui.model.CurrencyAndDate;
import com.example.android.myrates.ui.model.CurrencyRateItem;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class RateListFragment extends Fragment implements RateListViewImpl {

    private static final int NUMBER_OF_ROWS = 3;
    private int layoutId = R.layout.rate_grid_item;

    private TextView currentCurrencyTextView, informationRetrievedTextView, listLayoutTextView, gridLayoutTextView, allCurrenciesTextView, favoriteCurrenciesTextView;
    private ImageView currencyFlagImageView, filterImageView, changeBaseCurrencyImageView;
    private RecyclerView rateRecyclerView;
    private BottomNavigationView bottomNavigationView;
    private EditText searchedCurrency;

    private CurrencyAndDate currencyAndDate = new CurrencyAndDate();

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private ArrayList<CurrencyRateItem> currentList;

    private RateAdapter rateAdapter;

    private RateListPresenterImpl rateListPresenter;
    private SettingsViewImpl settingsFragment;

    private ViewGroup rootView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_rate_list, container, false);
        rateListPresenter = new RateListPresenter(this, currencyAndDate);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        rateListPresenter.getLatestRateList();
    }

    private void initViews() {
        filterImageView = requireView().findViewById(R.id.filter_icon);
        filterImageView.setSelected(false);
        filterImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int layoutId;

                if (!filterImageView.isSelected()) {
                    layoutId = R.layout.fragment_rate_list_filter_open;
                    filterImageView.setSelected(true);
                } else {
                    layoutId = R.layout.fragment_rate_list_filter_closed;
                    filterImageView.setSelected(false);
                }

                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(getContext(), layoutId);
                TransitionManager.beginDelayedTransition(rootView);
                constraintSet.applyTo((ConstraintLayout) rootView);
            }
        });

        searchedCurrency = requireView().findViewById(R.id.searched_currency);
        searchedCurrency.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rateAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        allCurrenciesTextView = requireView().findViewById(R.id.all_currencies_selector);
        allCurrenciesTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (favoriteCurrenciesTextView.isSelected())
                    favoriteCurrenciesTextView.setSelected(false);

                allCurrenciesTextView.setSelected(true);

                createAdapter(currentList, layoutId);

            }
        });

        favoriteCurrenciesTextView = requireView().findViewById(R.id.fav_currencies_selector);
        favoriteCurrenciesTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (allCurrenciesTextView.isSelected())
                    allCurrenciesTextView.setSelected(false);

                toggleNavBarVisibility(false);

                favoriteCurrenciesTextView.setSelected(true);

                createAdapter(rateListPresenter.loadFavoriteList(), layoutId);
            }
        });

        listLayoutTextView = requireView().findViewById(R.id.list_layout_selector);
        listLayoutTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rateRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                createAdapter(currentList, R.layout.rate_list_item);

                if (gridLayoutTextView.isSelected())
                    gridLayoutTextView.setSelected(false);

                listLayoutTextView.setSelected(true);

                sharedPreferences = requireContext().getSharedPreferences("sharedpreferences", Context.MODE_PRIVATE);
                editor = sharedPreferences.edit();
                editor.putString("LayoutManager", "linear");
                editor.apply();

            }
        });

        gridLayoutTextView = requireView().findViewById(R.id.grid_layout_selector);
        gridLayoutTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rateRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), NUMBER_OF_ROWS));
                createAdapter(currentList, R.layout.rate_grid_item);

                if (listLayoutTextView.isSelected())
                    listLayoutTextView.setSelected(false);

                gridLayoutTextView.setSelected(true);

                sharedPreferences = requireContext().getSharedPreferences("sharedpreferences", Context.MODE_PRIVATE);
                editor = sharedPreferences.edit();
                editor.putString("LayoutManager", "grid");
                editor.apply();

            }
        });

        currentCurrencyTextView = requireView().findViewById(R.id.current_currency);
        informationRetrievedTextView = requireView().findViewById(R.id.information_retrieved);
        currencyFlagImageView = requireView().findViewById(R.id.currency_flag);

        changeBaseCurrencyImageView = requireView().findViewById(R.id.base_currency_settings);
        changeBaseCurrencyImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsFragment = new SettingsFragment();
                settingsFragment.setCurrencyAndDate(currencyAndDate);
                getParentFragmentManager().beginTransaction()
                        .replace(R.id.container, (Fragment) settingsFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        bottomNavigationView = requireView().findViewById(R.id.nav_bar);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.favorite:
                        rateListPresenter.loadFavoriteList();
                        rateListPresenter.removeOrAddToFavorites(rateListPresenter.getCurrentlySelectedItem());
                        rateListPresenter.serializeArrayList();
                        toggleItemFavoriteIndicator(rateListPresenter.getCurrentlySelectedItem());
                        rateAdapter.notifyDataSetChanged();
                        break;

                    case R.id.calculator:
                        CalculatorViewImpl calculatorFragment = new CalculatorFragment();
                        calculatorFragment.setCurrencyRateItem(rateListPresenter.getCurrentlySelectedItem());
                        getParentFragmentManager().beginTransaction()
                                .replace(R.id.container, (Fragment) calculatorFragment)
                                .addToBackStack(null)
                                .commit();
                        break;

                    case R.id.history:
                        HistoryViewImpl historyFragment = new HistoryFragment();
                        historyFragment.setCurrencyRateItem(rateListPresenter.getCurrentlySelectedItem());
                        getParentFragmentManager().beginTransaction()
                                .replace(R.id.container, (Fragment) historyFragment)
                                .addToBackStack(null)
                                .commit();
                        break;

                }

                return false;
            }
        });

        rateRecyclerView = requireView().findViewById(R.id.rates);
        rateRecyclerView.setHasFixedSize(false);
        rateRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), NUMBER_OF_ROWS));
        sharedPreferences = requireContext().getSharedPreferences("sharedpreferences", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("LayoutManager", "grid");
        editor.apply();

        gridLayoutTextView.setSelected(true);
        allCurrenciesTextView.setSelected(true);
    }

    @Override
    public void onRatesListSuccess(CurrencyAndDate currencyAndDate, final ArrayList<CurrencyRateItem> currencyRateItems) {
        FetchCurrencyFlagUtils.loadImageCurrency(currencyFlagImageView, rateListPresenter.getBaseCurrency());

        informationRetrievedTextView.setText(String.format(getResources().getString(R.string.information_retrieved), rateListPresenter.getInformationDate()));
        currentCurrencyTextView.setText(String.format(getResources().getString(R.string.current_currency), rateListPresenter.getBaseCurrency()));

        currentList = currencyRateItems;
        createAdapter(currentList, layoutId);

    }

    @Override
    public void onRatesListFail(String errorMessage) {
        informationRetrievedTextView.setText(String.format(getResources().getString(R.string.information_retrieved), "UNAVAILABLE"));
        currentCurrencyTextView.setText(String.format(getResources().getString(R.string.current_currency), "UNAVAILABLE"));

        Toast.makeText(rootView.getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    private void createAdapter(ArrayList<CurrencyRateItem> currencyRateItems, int layoutId) {
        rateAdapter = new RateAdapter(currencyRateItems, layoutId);
        rateRecyclerView.setAdapter(rateAdapter);
        rateAdapter.setRateClickListener(new RateAdapter.RateClickListener() {
            @Override
            public void onRateClicked(CurrencyRateItem currency, boolean showNavigationBar) {
                rateListPresenter.setCurrentlySelectedItem(currency);
                toggleNavBarVisibility(showNavigationBar);
            }

        });

    }

    private void toggleNavBarVisibility(boolean showNavigationBar) {

        int animationId;
        boolean isBottomBarVisible = bottomNavigationView.getVisibility() == View.VISIBLE;

        if (!showNavigationBar) {
            animationId = R.anim.nav_bar_fade_out;
            isBottomBarVisible = false;
        } else {
            animationId = R.anim.nav_bar_fade_in;
        }

        if (!isBottomBarVisible) {
            Animation animation = AnimationUtils.loadAnimation(getContext(), animationId);
            bottomNavigationView.startAnimation(animation);
            bottomNavigationView.setVisibility(showNavigationBar ? View.VISIBLE : View.GONE);
        }

    }

    private void toggleItemFavoriteIndicator(CurrencyRateItem currencyRateItem) {

        if (!currencyRateItem.isCurrencyFavorite()) {
            currencyRateItem.setCurrencyFavorite(true);
        } else {
            currencyRateItem.setCurrencyFavorite(false);
        }

    }

}
