package com.example.android.myrates.ui.currencyanddate;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.example.android.myrates.core.SingletonContext;
import com.example.android.myrates.core.network.FetchDataTask;
import com.example.android.myrates.core.network.NetworkUtils;
import com.example.android.myrates.core.network.SimpleTask;
import com.example.android.myrates.core.json.JsonLatestStructure;
import com.example.android.myrates.ui.model.CurrencyAndDate;
import com.example.android.myrates.ui.model.CurrencyRateItem;
import com.example.android.myrates.utils.BuildUrlFromCurrency;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

public class RateListPresenter implements RateListPresenterImpl {

    private CurrencyAndDate currencyAndDate;
    private RateListViewImpl rateListViewImpl;

    private SharedPreferences sharedPreferences;
    private Gson gson = new Gson();

    private CurrencyRateItem currentlySelectedItem;
    private ArrayList<CurrencyRateItem> favoriteCurrencies;

    RateListPresenter(RateListViewImpl rateListViewImpl, CurrencyAndDate currencyAndDate) {
        this.currencyAndDate = currencyAndDate;
        this.rateListViewImpl = rateListViewImpl;
    }

    public CurrencyRateItem getCurrentlySelectedItem() {
        return currentlySelectedItem;
    }

    public void setCurrentlySelectedItem(CurrencyRateItem currentlySelectedItem) {
        this.currentlySelectedItem = currentlySelectedItem;
    }

    @Override
    public String getBaseCurrency() {
        return currencyAndDate.getDefaultCurrency();
    }

    @Override
    public String getInformationDate() {
        return currencyAndDate.getDateInformationRetrieved();
    }

    @Override
    public void getLatestRateList() {

        FetchDataTask fetchDataTask;

        final URL finalUrl = buildUrlFromBaseCurrency();

        fetchDataTask = new FetchDataTask(new SimpleTask() {
            @Override
            public String run() {

                String rates;

                try {
                    rates = NetworkUtils.getResponseFromHttpsUrl(Objects.requireNonNull(finalUrl));
                    return rates;


                } catch (IOException e) {
                    e.printStackTrace();
                    //rateListViewImpl.onRatesListFail("Failed to get a response from the API");
                }

                return null;
            }

            @Override
            public void workDone(String s) {

                if (TextUtils.isEmpty(s)) {
                    rateListViewImpl.onRatesListFail("Empty response from API");
                    return;
                }

                JsonLatestStructure jsonData = new Gson().fromJson(s, JsonLatestStructure.class);

                Map<String, Double> rateListItems = jsonData.getRates();
                ArrayList<CurrencyRateItem> arrayRates = new ArrayList<>();

                for (Map.Entry<String, Double> entry : rateListItems.entrySet()) {

                    String currency = entry.getKey();
                    double currencyVal = entry.getValue();

                    if (!currency.equals(jsonData.getBase()))
                        arrayRates.add(new CurrencyRateItem(currency, currencyVal, false));

                }

                currencyAndDate.setDefaultCurrency(jsonData.getBase());
                currencyAndDate.setDateInformationRetrieved(jsonData.getDate());

                rateListViewImpl.onRatesListSuccess(currencyAndDate, arrayRates);
            }

        });

        fetchDataTask.execute();

    }

    @Override
    public ArrayList<CurrencyRateItem> loadFavoriteList() {

        sharedPreferences = SingletonContext.get().getSharedPreferences("sharedpreferences", Context.MODE_PRIVATE);
        String favJson = sharedPreferences.getString("Favorites", null);
        Type type = new TypeToken<ArrayList<CurrencyRateItem>>() {
        }.getType();

        if (favoriteCurrencies == null) {

            if (sharedPreferences.getString("Favorites", null) == null)
                favoriteCurrencies = new ArrayList<>();

            else
                favoriteCurrencies = gson.fromJson(favJson, type);
        }

        return favoriteCurrencies;

    }

    @Override
    public void removeOrAddToFavorites(CurrencyRateItem currencyRateItem) {

        boolean currencyAlreadyAdded = false;

        for (CurrencyRateItem rateItem : favoriteCurrencies) {

            if (currencyRateItem.getCurrency().equals(rateItem.getCurrency())) {
                favoriteCurrencies.remove(rateItem);
                currencyAlreadyAdded = true;
                break;
            }

        }

        if (!currencyAlreadyAdded) {
            favoriteCurrencies.add(currencyRateItem);
        }

    }

    @Override
    public void serializeArrayList() {
        sharedPreferences = SingletonContext.get().getSharedPreferences("sharedpreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String favJson = gson.toJson(favoriteCurrencies);
        editor.putString("Favorites", favJson);
        editor.apply();
    }

    private URL buildUrlFromBaseCurrency() {

        sharedPreferences = SingletonContext.get().getSharedPreferences("com.example.android.myrates_preferences", Context.MODE_PRIVATE);
        String baseCurrency = sharedPreferences.getString("currency", "EUR");

        if (sharedPreferences == null)
            return BuildUrlFromCurrency.buildLatestRatesUrl(getBaseCurrency());
        else
            return BuildUrlFromCurrency.buildLatestRatesUrl(baseCurrency);
    }

}
