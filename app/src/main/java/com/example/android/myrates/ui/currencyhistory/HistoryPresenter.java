package com.example.android.myrates.ui.currencyhistory;

import android.annotation.SuppressLint;

import com.example.android.myrates.core.json.JsonHistoryStructure;
import com.example.android.myrates.core.network.FetchDataTask;
import com.example.android.myrates.core.network.NetworkUtils;
import com.example.android.myrates.core.network.SimpleTask;
import com.example.android.myrates.ui.model.CurrencyRateAndDateRetrieved;
import com.example.android.myrates.ui.model.CurrencyRateItem;
import com.example.android.myrates.utils.BuildUrlFromCurrency;
import com.github.mikephil.charting.data.Entry;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Objects;

public class HistoryPresenter implements HistoryPresenterImpl {

    private CurrencyRateItem currencyRateItem;

    private HistoryViewImpl historyView;

    private ArrayList<CurrencyRateAndDateRetrieved> currencyRateAndDateRetrieved;

    public HistoryPresenter(HistoryViewImpl historyView, CurrencyRateItem currencyRateItem) {
        this.historyView = historyView;
        this.currencyRateItem = currencyRateItem;
    }

    @Override
    public void fetchRatesHistoryForSelectedCurrency() {

        final URL url = BuildUrlFromCurrency.buildHistoricalRatesUrl(currencyRateItem.getCurrency());

        FetchDataTask fetchDataTask = new FetchDataTask(new SimpleTask() {
            @Override
            public String run() {

                String rates;

                try {
                    rates = NetworkUtils.getResponseFromHttpsUrl(Objects.requireNonNull(url));
                    return rates;
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            public void workDone(String s) {

                JsonHistoryStructure jsonData = new Gson().fromJson(s, JsonHistoryStructure.class);

                Map<String, Map<String, Double>> datesAndItems = jsonData.getRates();
                ArrayList<CurrencyRateAndDateRetrieved> arrayRates = new ArrayList<>();

                String currency = "", dateRetrieved;
                double currencyValue = 0;

                for (Map.Entry<String, Map<String, Double>> entry : datesAndItems.entrySet()) {

                    dateRetrieved = entry.getKey();
                    Map<String, Double> currencyValues = entry.getValue();

                    for (Map.Entry<String, Double> secEntry : currencyValues.entrySet()) {

                        currency = secEntry.getKey();
                        currencyValue = secEntry.getValue();

                    }

                    arrayRates.add(new CurrencyRateAndDateRetrieved(currency, dateRetrieved, currencyValue, 0));

                }

                currencyRateAndDateRetrieved = arrayRates;
                sortHistoryRatesByDate(arrayRates);

            }

        });

        fetchDataTask.execute();

    }

    @Override
    public void fetchRatesHistoryFromGivenDates(String initialDate, String finalDate) {

        final URL url = BuildUrlFromCurrency.historicalRatesFromGivenDates(currencyRateItem.getCurrency(), initialDate, finalDate);

        FetchDataTask fetchDataTask = new FetchDataTask(new SimpleTask() {
            @Override
            public String run() {

                String rates;

                try {
                    rates = NetworkUtils.getResponseFromHttpsUrl(Objects.requireNonNull(url));
                    return rates;
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            public void workDone(String s) {

                JsonHistoryStructure jsonData = new Gson().fromJson(s, JsonHistoryStructure.class);

                Map<String, Map<String, Double>> datesAndItems = jsonData.getRates();
                ArrayList<CurrencyRateAndDateRetrieved> arrayRates = new ArrayList<>();

                String currency = "", dateRetrieved;
                double currencyValue = 0;

                for (Map.Entry<String, Map<String, Double>> entry : datesAndItems.entrySet()) {

                    dateRetrieved = entry.getKey();
                    Map<String, Double> currencyValues = entry.getValue();

                    for (Map.Entry<String, Double> secEntry : currencyValues.entrySet()) {

                        currency = secEntry.getKey();
                        currencyValue = secEntry.getValue();

                    }

                    arrayRates.add(new CurrencyRateAndDateRetrieved(currency, dateRetrieved, currencyValue, 0));

                }

                sortHistoryRatesByDate(arrayRates);

            }

        });

        fetchDataTask.execute();

    }

    @Override
    public String getInformationFromPointOnGraph(float dateRetrieved, float rateValue) {

        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date((long) dateRetrieved));

        if (calendar.get(Calendar.HOUR) == 11) {
            calendar.add(Calendar.HOUR, 1);
            dateRetrieved = calendar.getTimeInMillis();
        }

        String s = dateFormat.format(dateRetrieved).toUpperCase();

        return s + " : " + rateValue;

    }

    @Override
    public long showSelectedRateOnGraph(CurrencyRateAndDateRetrieved rateAndDateRetrieved) {

        long dataItem = 0;

        for (int i = 0; i < currencyRateAndDateRetrieved.size(); i++) {

            try {
                if (currencyRateAndDateRetrieved.get(i).getDateRetrievedAsDate("yyyy-MM-dd").equals(rateAndDateRetrieved.getDateRetrievedAsDate("yyyy-MM-dd"))) {
                    dataItem = rateAndDateRetrieved.getDateRetrievedAsDate("yyyy-MM-dd").getTime();
                    break;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        return dataItem;

    }

    @Override
    public void sortHistoryRatesByDate(ArrayList<CurrencyRateAndDateRetrieved> arrayRates) {

        Collections.sort(arrayRates, new Comparator<CurrencyRateAndDateRetrieved>() {
            @Override
            public int compare(CurrencyRateAndDateRetrieved o1, CurrencyRateAndDateRetrieved o2) {
                return o1.getDateRetrieved().compareTo(o2.getDateRetrieved());
            }
        });

        addStatusAccordingToRate(arrayRates);

    }

    @Override
    public void addStatusAccordingToRate(ArrayList<CurrencyRateAndDateRetrieved> arrayRates) {

        for (int i = 0; i < arrayRates.size(); i++) {

            if (i != 0) {
                double currentRateStatus = arrayRates.get(i).getCurrencyValue();

                if (currentRateStatus > arrayRates.get(i - 1).getCurrencyValue()) {
                    arrayRates.get(i).setRateStatus(1);
                } else if (currentRateStatus == arrayRates.get(i - 1).getCurrencyValue()) {
                    arrayRates.get(i).setRateStatus(2);
                } else if (currentRateStatus < arrayRates.get(i - 1).getCurrencyValue()) {
                    arrayRates.get(i).setRateStatus(3);
                }

            }

        }

        historyView.onHistoryRetrieved(arrayRates);

    }

    @Override
    public ArrayList<Entry> createDataSetFromRates(ArrayList<CurrencyRateAndDateRetrieved> arrayRates) {

        ArrayList<Entry> generatedDataSet = new ArrayList<>();

        for (CurrencyRateAndDateRetrieved rateAndDateRetrieved : arrayRates) {
            try {
                long curDate = rateAndDateRetrieved.getDateRetrievedAsDate("yyyy-MM-dd").getTime();
                float curVal = (float) rateAndDateRetrieved.getCurrencyValue();
                generatedDataSet.add(new Entry(curDate, curVal));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return generatedDataSet;

    }

}
