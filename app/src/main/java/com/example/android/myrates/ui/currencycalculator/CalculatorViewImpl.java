package com.example.android.myrates.ui.currencycalculator;

import com.example.android.myrates.ui.model.CurrencyRateItem;

import java.util.ArrayList;

public interface CalculatorViewImpl {

    void setCurrencyRateItem(CurrencyRateItem currencyRateItem);

    void onRatesRetrieved(ArrayList<CurrencyRateItem> currencyRateItems);

}
