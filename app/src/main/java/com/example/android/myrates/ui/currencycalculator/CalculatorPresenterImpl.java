package com.example.android.myrates.ui.currencycalculator;

import androidx.recyclerview.widget.RecyclerView;

import com.example.android.myrates.ui.model.CurrencyRateItem;

import java.util.ArrayList;

public interface CalculatorPresenterImpl {

    void fetchRatesForSelectedCurrency();

    boolean setLayoutManager(RecyclerView layoutManager);

}
