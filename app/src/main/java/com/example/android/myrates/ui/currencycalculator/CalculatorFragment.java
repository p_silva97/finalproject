package com.example.android.myrates.ui.currencycalculator;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.myrates.R;
import com.example.android.myrates.ui.model.CurrencyRateItem;
import com.example.android.myrates.utils.FetchCurrencyFlagUtils;

import java.util.ArrayList;
import java.util.Objects;

public class CalculatorFragment extends Fragment implements CalculatorViewImpl {

    private CurrencyRateItem currencyRateItem;

    private ArrayList<CurrencyRateItem> calculatedCurrencyRateItems, updatedCurrencyRateItems;

    private ImageView currencyCalculatorImageView;
    private RecyclerView calculatorRecyclerView;
    private EditText currencyCalculatorEditText;

    private CalculatedRatesAdapter calculatedRatesAdapter;
    private CalculatorPresenter calculatorPresenter;

    private ViewGroup rootView;

    public void setCurrencyRateItem(CurrencyRateItem currencyRateItem) {
        this.currencyRateItem = currencyRateItem;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_calculator, container, false);
        calculatorPresenter = new CalculatorPresenter(this, currencyRateItem);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        calculatorPresenter.fetchRatesForSelectedCurrency();
    }

    private void initViews() {

        currencyCalculatorImageView = requireView().findViewById(R.id.calculator_currency);
        FetchCurrencyFlagUtils.loadImageCurrency(currencyCalculatorImageView, currencyRateItem.getCurrency());

        currencyCalculatorEditText = requireView().findViewById(R.id.editText);
        currencyCalculatorEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s == null || s.length() == 0) {
                    createAdapter(calculatedCurrencyRateItems);
                } else {

                    updatedCurrencyRateItems = new ArrayList<>();

                    for (int i = 0; i < calculatedCurrencyRateItems.size(); i++) {
                        String curName = calculatedCurrencyRateItems.get(i).getCurrency();
                        double newValue = calculatedCurrencyRateItems.get(i).getCurrencyValue() * Integer.parseInt(s.toString());
                        CurrencyRateItem newObj = new CurrencyRateItem(curName, newValue, false);
                        updatedCurrencyRateItems.add(newObj);
                    }

                    createAdapter(updatedCurrencyRateItems);

                }
                calculatedRatesAdapter.notifyDataSetChanged();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        calculatorRecyclerView = requireView().findViewById(R.id.calculator_rates);
        calculatorRecyclerView.setHasFixedSize(false);
        calculatorRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    public void onRatesRetrieved(ArrayList<CurrencyRateItem> currencyRateItems) {
        calculatedCurrencyRateItems = currencyRateItems;
        createAdapter(currencyRateItems);
    }

    private void createAdapter(ArrayList<CurrencyRateItem> currencyRateItems) {

        if (calculatorPresenter.setLayoutManager(calculatorRecyclerView)) {
            calculatedRatesAdapter = new CalculatedRatesAdapter(currencyRateItems, R.layout.rate_grid_item);
        } else {
            calculatedRatesAdapter = new CalculatedRatesAdapter(currencyRateItems, R.layout.rate_list_item);
        }
        calculatorRecyclerView.setAdapter(calculatedRatesAdapter);

    }

}