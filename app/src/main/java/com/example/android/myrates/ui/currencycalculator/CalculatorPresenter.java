package com.example.android.myrates.ui.currencycalculator;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.myrates.core.SingletonContext;
import com.example.android.myrates.core.json.JsonLatestStructure;
import com.example.android.myrates.core.network.FetchDataTask;
import com.example.android.myrates.core.network.NetworkUtils;
import com.example.android.myrates.core.network.SimpleTask;
import com.example.android.myrates.ui.model.CurrencyRateItem;
import com.example.android.myrates.utils.BuildUrlFromCurrency;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

public class CalculatorPresenter implements CalculatorPresenterImpl {

    private CurrencyRateItem currencyRateItem;

    private CalculatorViewImpl calculatorView;

    CalculatorPresenter(CalculatorViewImpl calculatorView, CurrencyRateItem currencyRateItem) {
        this.calculatorView = calculatorView;
        this.currencyRateItem = currencyRateItem;
    }

    @Override
    public void fetchRatesForSelectedCurrency() {

        final URL url = BuildUrlFromCurrency.buildLatestRatesUrl(currencyRateItem.getCurrency());

        FetchDataTask fetchDataTask = new FetchDataTask(new SimpleTask() {
            @Override
            public String run() {

                String rates;

                try {
                    rates = NetworkUtils.getResponseFromHttpsUrl(Objects.requireNonNull(url));
                    return rates;
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            public void workDone(String s) {

                /*if (TextUtils.isEmpty(s)) {
                    rateListViewImpl.onRatesListFail("Empty response from API");
                    return;
                }*/

                JsonLatestStructure jsonData = new Gson().fromJson(s, JsonLatestStructure.class);

                Map<String, Double> rateListItems = jsonData.getRates();
                ArrayList<CurrencyRateItem> arrayRates = new ArrayList<>();

                for (Map.Entry<String, Double> entry : rateListItems.entrySet()) {

                    String currency = entry.getKey();
                    double currencyVal = entry.getValue();

                    if (!currency.equals(currencyRateItem.getCurrency()))
                        arrayRates.add(new CurrencyRateItem(currency, currencyVal, false));

                }

                calculatorView.onRatesRetrieved(arrayRates);

            }

        });

        fetchDataTask.execute();

    }

    @Override
    public boolean setLayoutManager(RecyclerView layoutManager) {

        SharedPreferences sharedPreferences = SingletonContext.get().getSharedPreferences("sharedpreferences", Context.MODE_PRIVATE);
        String layout = sharedPreferences.getString("LayoutManager", "linear");

        if (layout.equals("grid")) {
            layoutManager.setHasFixedSize(false);
            layoutManager.setLayoutManager(new GridLayoutManager(layoutManager.getContext(), 3));
            return true;
        } else {
            layoutManager.setHasFixedSize(false);
            layoutManager.setLayoutManager(new LinearLayoutManager(layoutManager.getContext()));
            return false;
        }

    }

}
